#+startup: showall
#+title: Train Reservations
#+author: rdgoite

* Day 0
  + KIX -> Shin-Osaka [Limited Express Haruka]
    - *No. 22: 12:14 - 13:05*
    - No. 24: 12:44 - 13:35
  + Shin-Osaka -> Nagoya [Shinkansen]
    - Hikari 480: 18:40 - 19:33
  + Nagoya -> Takayama [Limited Express Hida]
    - No. 19: 19:43 - 22:15
     
* Day 1
  + Takayama -> Nagoya [Limited Express Hida]
    - No. 20: 18:46 - 21:02
  + Nagoya -> Shin-Osaka [Shinkansen]
    - Hikari 529: 21:11 - 22:16

* Day 3
  + Kyoto -> Tokyo [Shinkansen]
    - Hikari 480: 18:56 - 21:40

* Day 4
  + Shinagawa -> Shin-Yokohama [Shinkansen]
    - Hikari 479: 16:10 - 16:21
  
* Day 5
  + Tokyo -> Atami [Shinkansen]
    - Kodama 639: 08:26 - 09:14
  + Atami to Kawazu [Limited Express Odoriko]
    - No. 103: 09:19 - 10:32
  + Kawazu -> Tokyo [Limited Express Odoriko]
    - No. 184: 12:48 - 15:10
