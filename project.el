;; run this buffer to add project to the org publish list
(setq org-publish-project-alist
      '(("japan-2017-02"
	:base-directory "."
	:publishing-directory "./www"
	:publishing-function org-html-publish-to-html
	;; author not working for some reason
	:with-author "rdgoite"
	:section-numbers nil
	:with-toc nil))
      )
